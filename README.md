# Docker.
## Requirements.

Linux. 

Docker 20.10.2+. 

Docker compose 1.25.0+.

## Install soft.
```
    sudo apt update && apt upgrade
    sudo apt install docker docker.io containerd docker-compose git zip
```

## Get docker.
```
    mkdir /var/iblockchaingroup && cd /var/iblockchaingroup
    git clone git@gitlab.com:iblockchaingroup/docker.git
```

##	Build.
```
    cd /var/iblockchaingroup/docker
    mkdir git && cd git && git clone https://github.com/tradingsecret/beam.git && git clone https://github.com/tradingsecret/blockex.git && git clone git@gitlab.com:iblockchaingroup/faucet.git
    mkdir -p data_main/blockchain && chown -R 1000:1000 data_main/blockchain
    mkdir -p data_main/storage && chown -R 82:82 data_main/storage
    
    sudo docker-compose -f build.yml build
```

## Push.
```
    docker login -u tradingsecret
    docker push tradingsecret/blockchain_node
    docker push tradingsecret/blockchain_miner_opencl
    docker push tradingsecret/blockchain_blockex
    docker push tradingsecret/blockchain_blockex_frontend
    docker push tradingsecret/blockchain_blockex_nginx
    docker push tradingsecret/blockchain_faucet_api
```

## Run dev.
```
    #at first run uncomment init command the comment.
    sudo docker-compose -f compose-blockex.dev.yml -f compose-frontend.dev.yml up
```

## Initialize.
```
    cd /var/iblockchaingroup
    mkdir -p data_main/blockchain && chown -R 1000:1000 data_main/blockchain
    
    cd docker
    sudo docker-compose -f stack.yml run blockchain_master_node bash
    ./wallet/cli/beam-wallet-masternet --pass="secret^&^73pass674368" --wallet_path=/app/.blockchain/mining_wallet.db --confirmations_count=1 init
    ./wallet/cli/beam-wallet-masternet --pass="secret^&^73pass674368" --wallet_path=/app/.blockchain/mining_wallet.db --confirmations_count=1 export_miner_key --subkey=1
    ./wallet/cli/beam-wallet-masternet --pass="secret^&^73pass674368" --wallet_path=/app/.blockchain/mining_wallet.db export_owner_key
    ./wallet/cli/beam-wallet-masternet treasury --wallet_path=/app/.blockchain/mining_wallet.db --pass="secret^&^73pass674368" --tr_pecents=1 --tr_pecents_total=0 --tr_comment=ARC --tr_op=0
    #set tr_wid and run in while with tr_op 1..4
    ./wallet/cli/beam-wallet-masternet treasury --wallet_path=/app/.blockchain/mining_wallet.db --tr_wid=fbfe2f83aacd60f06d09ebc0a95ab773ad27457ec64bd36f4d670662207b7241 --pass="secret^&^73pass674368" --tr_pecents=1 --tr_pecents_total=0 --tr_comment=ARC --tr_op=1
    mv treasury_data.bin .blockchain/
    #than set new TreasuryCehcksum and recompile and recreate mining wallet
    
    ./wallet/cli/beam-wallet-masternet --node_addr=23.88.54.192:10000  --pass="secret^&^73pass674368" --wallet_path=/app/.blockchain/mining_wallet.db listen
    
    #write tr hash
    333dbf7b03beda8672e6c6a7027d1fdc68d059569044fbc59275b42f686c4f75
    
    openssl req -x509 -newkey rsa:4096 -keyout beam-stratum-key.pem -out beam-stratum-crt.pem -days 3650 -nodes -subj '/CN=localhost'
    mv beam-stratum-key.pem /app/.blockchain/stratum.key
    mv beam-stratum-crt.pem /app/.blockchain/stratum.crt
```

##	Deploy.
```
    sudo docker stack deploy -c stack.yml blockchain
    sudo docker stack deploy -c stack_frontend.yml blockchain_frontend
    sudo docker stack deploy -c stack_node.yml blockchain_node
    
    #update replicas
    sudo docker service update --replicas 30 blockchain_blockchain_node
    sudo docker service update --force --image tradingsecret/blockchain_miner_opencl:latest blockchain_blockchain_miner_opencl
    
    #test only.
    sudo docker-compose -f stack.yml up
    sudo docker-compose -f stack_frontend.yml up
    sudo docker-compose -f stack_node.yml up
    
    #test only.
    sudo docker stack rm blockchain
```

## Miner.
```
apt install ubuntu-drivers-common
sudo apt install ocl-icd-opencl-dev
ubuntu-drivers devices
ubuntu-drivers autoinstall

nohup ./lolMiner --coin BEAM --pool 66.29.138.190:3333 --user 29a5d136a80bb7bae9aa2784e913d8bfa65da1ab2844e7c8cd550d8a62423e9450e &
```

## Update.
```
    sudo docker service update --replicas 0 blockchain_frontend_blockchain_redis
    sudo docker service update --replicas 0 --image tradingsecret/blockchain_blockex blockchain_frontend_blockchain_blockex
    docker system prune --force
    
    sudo docker service update --replicas 1 blockchain_frontend_blockchain_redis
    sudo docker service update --replicas 1 --image tradingsecret/blockchain_blockex blockchain_frontend_blockchain_blockex
    
    sudo docker service update --replicas 1  --image tradingsecret/blockchain_blockex_nginx blockchain_frontend_blockchain_blockex_nginx
    sudo docker service update --replicas 1  --image tradingsecret/blockchain_faucet_api blockchain_frontend_blockchain_faucet_api
```

## Add miner node.
```
    docker node ls
    docker node update --label-add node=yes ID
    docker node update --label-add miner=yes ID
```

## Add to crontab (it will decide question with stratum server and miner).
```
0 0 * * * docker service update --replicas 0 blockchain_blockchain_master_node
1 0 * * * docker service update --replicas 1 blockchain_blockchain_master_node
```
