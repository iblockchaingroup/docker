FROM node:14-alpine

RUN apk update && \
    apk upgrade && \
    apk add -U tzdata alpine-sdk python2

ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir app
WORKDIR /app

COPY git/blockex .
RUN cd frontend && npm install && npm run build

