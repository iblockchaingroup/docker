#bash

celery -A blockex beat --detach
celery -A blockex worker --detach
daphne -b 0.0.0.0 -p 8000 blockex.asgi:application --access-log access.log
