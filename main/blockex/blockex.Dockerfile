#FROM ubuntu:20.04
FROM python:3.7-buster

ENV DEBIAN_FRONTEND="noninteractive"

#RUN apt update && apt upgrade -y && \
#    apt install -y git python3-pip python3-venv iputils-ping net-tools telnet python3-wheel wget python3-dev

RUN apt update && apt upgrade -y && \
    apt install -y git iputils-ping net-tools telnet wget

ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN mkdir app
WORKDIR app

COPY git/blockex .

RUN pip3 install wheel && \
    pip3 install -r requirements.txt && \
    pip3 install gunicorn && \
    python3 manage.py makemigrations && \
    python3 manage.py migrate && \
    python3 manage.py collectstatic && \
    python3 -m pip install uwsgi

COPY main/blockex/start.sh .
