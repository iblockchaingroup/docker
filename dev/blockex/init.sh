#bash

mkdir venvs
python3 -m venv venvs/blockex
. venvs/blockex/bin/activate
pip3 install -r requirements.txt
pip3 install gunicorn

python3 manage.py makemigrations
python3 manage.py migrate
